from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.forms import TaskForm
from tasks.models import Task


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            task = form.save()
            return redirect("list_projects", id=task.id)
    else:
        form = TaskForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/create_task.html", context)


@login_required
def show_my_task(request):
    task = Task.objects.filter(assignee=request.user)
    context = {
        "task": task,
    }
    return render(request, "tasks/my_task.html", context)

    #  def show_receipt(request):
    # receipts = Receipt.objects.all()
    # context = {
    #     "receipts": receipts,
    # }
    # return render(request, "receipts/receipts.html", context)
